\documentclass{beamer}

\usepackage{fontawesome5}
\usepackage{tikz-cd}
\usepackage{listings}

%\usetheme{Berkeley}
\usetheme[numbering=none, progressbar=frametitle, titleformat=smallcaps]{metropolis}
\usecolortheme[snowy]{owl}
%\usecolortheme{owl}

\setbeamercovered{invisible}
\setbeamercovered{again covered={\opaqueness<1->{15}}}

\title{Timestamping with OpenTimestamps}
\subtitle{FOSDEM, 2024}
\author{Timothy M. Redaelli}
\institute{
  \vfill
  \href{mailto:timothy@fsfe.org}{\faIcon{envelope} timothy@fsfe.org}\hfill
  \href{https://t.me/THREADelli}{\faIcon{telegram} @threadelli}\hfill
  \href{https://twitter.com/THREADelli}{\faIcon{twitter} @threadelli}\hfill
  \href{https://gitlab.com/drizzt}{\faIcon{gitlab} drizzt}\hfill
  \href{https://github.com/drizzt}{\faIcon{github} drizzt}
}

\date{2024/02/03}

\titlegraphic{
  \hfill
  %\includegraphics[width=2cm]{fsfe_logo}
  \includegraphics[width=2cm]{opentimestamps_logo}
}

%\AtBeginSubsection[]
%{
%  \begin{frame}
%    \frametitle{Nelle prossime slide}
%    %\tableofcontents[currentsection,currentsubsection,sectionstyle=show/hide, subsectionstyle=show/shaded/hide]
%    \tableofcontents[currentsection,currentsubsection]
%  \end{frame}
%}

\begin{document}
  \maketitle

  \begin{frame}
  \frametitle{Index}
  \scriptsize \tableofcontents
  \end{frame}

  \section{Introduction to Timestamping}
  \subsection{What is timestamping?}
  \begin{frame}{What is timestamping?}
    \begin{itemize}[<+>]
      \item Ascertaining an accurate date on a document
      \item (Italian) law requires that dates on important documents be ascertained by a public official
      \item What about digital documents?
    \end{itemize}
  \end{frame}
  \subsection{What is digital timestamping?}
  \begin{frame}{What is digital timestamping?}
    \begin{itemize}[<+>]
      \item Based on a third-party digital signature
      \item Based on a certification authority
    \end{itemize}
  \end{frame}

  \section{Timestamping and Blockchain}
  \subsection{How can we use the Blockchain for timestamping?}
  \begin{frame}{How can we use the Blockchain for timestamping?}
    \only<1>{
      \begin{tikzcd}[ampersand replacement=\&]
        \& \& \tikz \path node [draw, align=center] {File\\Hash}; \arrow[d]
        \\
        \tikz \path node [draw] {$t_0$};
        \& \tikz \path node [draw] {$t_1$}; \arrow[l]
        \& \tikz \path node [draw] {$t_2$}; \arrow[l]
      \end{tikzcd}
    }
    \only<2>{
      \begin{tikzcd}[ampersand replacement=\&]
        \& \& \tikz \path node [draw, align=center] {File\\Hash}; \arrow[d]
        \\
        \tikz \path node [draw] {$t_0$};
        \& \tikz \path node [draw] {$t_1$}; \arrow[l]
        \& \tikz \path node [draw] {$t_2$}; \arrow[l]
        \& \tikz \path node [draw] {\color{red} $t_3$}; \arrow[l]
        \& \tikz \path node [draw] {\color{purple} $t_4$}; \arrow[l]
      \end{tikzcd}
    }
  \end{frame}
  \subsection{Why the Blockchain?}
  \begin{frame}{Why the Blockchain?}
    \begin{itemize}[<+>]
      \item Safe
      \item Open
      \item Cheap
    \end{itemize}
  \end{frame}

  \section{OpenTimestamps}
  \subsection{Why OpenTimestamps?}
  \begin{frame}{Why OpenTimestamps?}
    \begin{itemize}
      \item<1-2> Blockchain is \textit{permissionless} (open)
      \begin{itemize}
        \item<2> Anyone could do timestamping directly
      \end{itemize}
      \item<3-5> OpenTimestamps is a standard way of doing timestamping \textit{trustless} (trust no one)
      \begin{itemize}
        \item<4> Proposed by Peter Todd (Bitcoin Core developer)
        \item<5> Used by dozens of different companies
      \end{itemize}
      \item<6-7> It's \textit{almost} infinitely scalable
      \begin{itemize}
        \item<7> Merkle Tree
      \end{itemize}
    \end{itemize}
  \end{frame}
  \begin{frame}[standout]{Merkle Tree}
    \includegraphics[width=\textwidth]{Merkle_Tree}
  \end{frame}
  \subsection{Usage}
  \begin{frame}{Usage}
    OTS provides users multiple and easy ways to create and independently verify timestamps:

      \begin{itemize}
        \item With \href{https://github.com/opentimestamps/opentimestamps-client}{opentimestamps-client} in Python
        \item With \href{https://github.com/opentimestamps/java-opentimestamps}{java-opentimestamps}
        \item With \href{https://github.com/opentimestamps/javascript-opentimestamps}{javascript-opentimestamps}
        \item (In the future) With \href{https://github.com/opentimestamps/rust-opentimestamps}{rust-opentimestamps}
        \item On \href{https://opentimestamps.org}{opentimestamps.org}
      \end{itemize}

    In the following slides it is shown an example of the usage of the Python client.
  \end{frame}
  \begin{frame}[allowframebreaks,fragile]{Timestamp creation}
    The stamp operation creates the first version of the timestamp. It is applied to the file for which you want to prove its existence (original file).\\

    \begin{lstlisting}[breaklines,frame=single]
$ cat hello.txt
Hello World!
$ ots stamp hello.txt
Submitting to remote calendar https://a.pool.opentimestamps.org
Submitting to remote calendar https://b.pool.opentimestamps.org
Submitting to remote calendar https://a.pool.eternitywall.com
    \end{lstlisting}

    The stamp operation calculates the SHA256 hash of the original file, concatenates a random 128-bit nonce to maintain privacy, and recalculates the SHA256 hash, sending this unique value to the calendar servers.

    Each of the calendar servers will add the received hash to its Merkle tree and return the necessary response to generate the initial OTS file. This OTS file is still incomplete because it does not yet contain the record in the blockchain.\\

    Once a reasonable time has elapsed, the user will run the upgrade operation on the same OTS file. This will communicate with the calendar servers and update the OTS file with the Bitcoin block header attestation.

    \begin{lstlisting}[breaklines,frame=single]
$ ots upgrade hello.txt.ots
Success! Timestamp complete
    \end{lstlisting}

    It is also possible to create timestamps for several different files simultaneously. In that case, the stamp operation will send a single request to the calendar servers with a Merkle root derived from the original files, and later, that same operation will calculate the Merkle tree paths and create the timestamps for each one of the original files.
  \end{frame}
  \begin{frame}[fragile]{Timestamp verification}
    The verification of the OTS proof requires both the OTS file and the original file. The user must also have an up-to-date Bitcoin node (it is not strictly required a full node, indeed the attestation are on the block header, thus a pruned node is enough to verify a timestamp) on their own machine to perform the verification without relying on trusted third parties.

    \begin{lstlisting}[breaklines,frame=single]
$ ots verify hello.txt.ots
Assuming target filename is 'hello.txt'
Success! Bitcoin attests data existed as of Mon Apr 16 01:15:16 2018 CEST
    \end{lstlisting}
  \end{frame}

  \begin{frame}[fragile]{Show timestamp information}
    The basic structure of a timestamp is divided into three main sections:
    \begin{enumerate}
      \item File hash
      \item Merkle tree construction
      \item Bitcoin block header attestation
    \end{enumerate}
    The timestamp is saved in a binary file to save space and avoid problems of interpretation, encoding and compatibility between systems. Generally, this file has a .ots extension and its magic number is
    \begin{lstlisting}[breaklines]
\x00OpenTimestamps\x00\x00Proof\x00\xbf\x89\xe2\xe8\x84\xe8\x92\x94
    \end{lstlisting}
  \end{frame}

  %\subsection{Questions}
  %\begin{frame}[standout]
  %  Questions?
  %\end{frame}

  \subsection{Examples of OpenTimestamps usage}
  \begin{frame}{Examples of OpenTimestamps usage}
    \begin{itemize}
      \item \href{https://opentimestamps.org}{opentimestamps.org}
      \item \href{https://proofmode.org}{proofmode.org}
      \item \href{https://ansiacheck.belloworld.it}{ansiacheck.belloworld.it}
      \item \href{https://tweetstamp.org}{tweetstamp.org}
    \end{itemize}
  \end{frame}
  %\subsection{Demo}
  %\begin{frame}[standout]
  %  Demo
  %\end{frame}

  \section{The End}
  \subsection{Questions}
  \begin{frame}[standout]
    At the end of this lightning talk, if you have any question or would like
    to discuss futher, feel free to reach out to me directly.\\
    I'll be available for conversations after the session.\\
    Thank you!
  \end{frame}
  \subsection{Summary}
  \begin{frame}{Summary}
    For more information see \href{https://opentimestamps.org}{opentimestamps.org}.

    \usebeamertemplate*{institute}
  \end{frame}
\end{document}
